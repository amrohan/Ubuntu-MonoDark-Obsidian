# Ubuntu-MonoDark-Obsidan

## Screnshots

![Obsidian SS](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/m2xl06zozlpj9fj6p5hp.png)

![MonoDark Obsidain](https://dev-to-uploads.s3.amazonaws.com/uploads/articles/hl7y6sbbs1dihztdywn2.png)

### Installation

1. Download MonoDark.css to your Obsidian vault folder
2. In Obsidian, click Settings -> Plugins make sure `Custom CSS` is enabled
3. Pick either light or dark theme in Obsidian's appearance settings.

>Note: .obidian is hidden folder
#### If you wish to change the accent colour from pink to a different colour, go to the `.obsidian/themes` folder
